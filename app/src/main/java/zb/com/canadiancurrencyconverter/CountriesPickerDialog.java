package zb.com.canadiancurrencyconverter;


import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by zohaibhussain on 2016-05-12.
 */
public class CountriesPickerDialog extends android.support.v4.app.DialogFragment {

    private static final String COUNTRIES_LIST = "Countries List";

    @BindView(R.id.countriesRecyclerView)
    protected RecyclerView mCountriesRecyclerView;

    @BindView(R.id.currency_search)
    protected android.widget.SearchView mCountrySearch;

    @BindView(R.id.cancelButton)
    protected Button mCancelButton;

    private List<Country> mCountries;
    private DialogCallback mListener;


    public static CountriesPickerDialog newInstance(List<Country> countries){
        CountriesPickerDialog dialog = new CountriesPickerDialog();
        Bundle bundle = new Bundle();
        bundle.putSerializable(COUNTRIES_LIST, (ArrayList)countries);
        dialog.setArguments(bundle);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            dialog.setStyle(STYLE_NORMAL, android.R.style.Theme_Material_Light_Dialog_Alert);
        }
        //dialog.setStyle(DialogFragment.STYLE_NO_INPUT, R.style.countriesDialog);
        return dialog;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof DialogCallback)
            setListener((DialogCallback) activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        setListener(null);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCountries = (ArrayList) getArguments().getSerializable(COUNTRIES_LIST);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.countries_picker, container, false);
        ButterKnife.bind(this, view);
        mCountriesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mCountriesRecyclerView.setAdapter(new CountriesAdapter(mCountries));
        mCountriesRecyclerView.setHasFixedSize(true);
        getDialog().setTitle("Select Country");
        mCountrySearch.setOnQueryTextListener(new android.widget.SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Filter list based on new text, bind list to adapter and notify ds changed
                if (newText.equals(""))
                    mCountriesRecyclerView.setAdapter(new CountriesAdapter(mCountries));
                else
                    ((CountriesAdapter)mCountriesRecyclerView.getAdapter()).
                            setFilteredList(Countries.getFilteredCountriesList(newText));
                return false;
            }
        });
        mCountrySearch.setOnCloseListener(new android.widget.SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                mCountriesRecyclerView.setAdapter(new CountriesAdapter(mCountries));
                return false;
            }
        });
        return view;
    }


    @OnClick(R.id.cancelButton)
    protected void onCancelButtonClicked(){
        this.dismiss();
    }

    public interface DialogCallback{
        void onCountrySelected(Country country);
    }

    public void setListener(DialogCallback listener) {
        mListener = listener;
    }

    protected class CountriesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        @BindView(R.id.country_flag)
        protected CircleImageView mCountryFlag;

        @BindView(R.id.country_name)
        protected TextView mCountryName;

        private Country mCountry;

        public CountriesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }

        public void bind(Country country) {
            mCountry = country;
            //mCountryFlag.setImageResource(mCountry.getFlag());
            Picasso.with(getActivity()).
                    load(mCountry.getFlag()).
                    into(mCountryFlag);
            String countryText = mCountry.getCountryCurrencyText();
            mCountryName.setText(countryText);
        }

        @Override
        public void onClick(View v) {
            if(mListener != null)
                mListener.onCountrySelected(mCountry);
            dismiss();
        }
    }

    private class CountriesAdapter extends RecyclerView.Adapter<CountriesViewHolder> {

        List<Country> countriesList;

        public CountriesAdapter(List<Country> countriesList) {
            super();
            this.countriesList = countriesList;
        }

        @Override
        public CountriesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new CountriesViewHolder(LayoutInflater.from(getActivity()).inflate(R.layout.country_picker_row, parent, false));
        }

        @Override
        public void onBindViewHolder(CountriesViewHolder holder, int position) {
            holder.bind(countriesList.get(position));
        }

        @Override
        public int getItemCount() {
            return countriesList.size();
        }

        public void setFilteredList(List<Country> filteredList){
            this.countriesList = filteredList;
            notifyDataSetChanged();
        }
    }


}
