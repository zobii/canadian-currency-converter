package zb.com.canadiancurrencyconverter;

import android.animation.ObjectAnimator;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import butterknife.OnTextChanged;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import zb.com.canadiancurrencyconverter.api.RatesResponse;
import zb.com.canadiancurrencyconverter.api.CurrencyAPIEndpointsInterface;
import zb.com.canadiancurrencyconverter.api.models.Rates;

public class MainActivity extends AppCompatActivity implements CountriesPickerDialog.DialogCallback {

    //public static final String URL_BASE = "http://api.fixer.io/";
    public static final String URL_BASE = "http://apilayer.net/api/";

    public static final String ACCESS_KEY = "a1d3e380551423eee1ca469d4769d2eb";
    private static final String TAG = "POC";
    //Dialog types
    public static final int FETCH_RATES = 1;
    public static final int FAIL_FIRST_TIME= 2;
    public static final int FAIL_OLD_RATES= 3;
    public static final int EXIT_APP= 4;

    public static final String BASE_COUNTRY = "CAD";
    public static final long EXPIRY_TIME = 86400000; //1 day in milliseconds
    public static final String CURRENCY_PICKER_TAG = "currency_pcier";
    public static final String PLAYSTORE_BROWSER_LINK = "https://play.google.com/store/apps/details";

    @BindView(R.id.baseCountryImage)
    CircleImageView mBaseCountryImage;

    @BindView(R.id.baseCountryAmount)
    TextView mBaseCountryAmount;

    @BindView(R.id.baseCountryCurrency)
    TextView mBaseCountryCurrency;

/*
    @BindView(R.id.awayCountryContainer)
    RelativeLayout mAwayCountryCountainer;
*/

    @BindView(R.id.awayCountryImage)
    CircleImageView mAwayCountryImage;

    @BindView(R.id.awayCountryCurrency)
    TextView mAwayCountryCurrency;

    @BindView(R.id.awayCountryAmount)
    TextView mAwayCountryAmount;

    @BindView(R.id.date_rate)
    TextView mRateDate;

    @BindView(R.id.clear_button)
    ImageButton mClearButton;

    @BindView(R.id.dot_button)
    Button mDotButton;

    @BindView(R.id.switcher)
    ImageButton mSwitcher;

    CurrencyAPIEndpointsInterface mApiService;
    Rates mCurrentRates;
    Country mBaseCountry;
    Country mAwayCountry;
    Countries mCountries;
    CurrencyPrefs mPreferences;
    boolean mSwitched;
    AlertDialog.Builder mDialogBuilder;
    AlertDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initService();
        mCountries = new Countries();
        mPreferences = CurrencyPrefs.getInstance(this);
        mSwitched = mPreferences.getCurrenciesFlipped();
        //bindBaseCountry();
        //TODO try putting this in the show dialog methodx
        mDialogBuilder = new AlertDialog.Builder(this);
        if (mPreferences.isFirstTime() || isratesExpired(mPreferences.getRatesDate()))
            fetchRatesServiceCall();
    }

    private void restoreFromPrefs() {
        bindDate(mPreferences.getRatesDate());
        mCurrentRates = mPreferences.getRates();
        bindAwayCountry(mCountries.getCountryByCurrencyCode(mPreferences.getAwayCountry()));
        bindBaseCountry();
        if (mSwitched)
            switchCountries();
        mBaseCountryAmount.setText(mPreferences.getAmount());
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!mPreferences.isFirstTime() && !isratesExpired(mPreferences.getRatesDate())){
            restoreFromPrefs();
        }
        //todo fix
/*        else if () {
            fetchRatesServiceCall();
        }*/
    }

    private void fetchRatesServiceCall() {
        //TODO Show dialog saying fetching rates
        showAlertDialog(FETCH_RATES);
        mApiService.getRates(ACCESS_KEY).enqueue(new Callback<RatesResponse>() {
            @Override
            public void onResponse(Call<RatesResponse> call, Response<RatesResponse> response) {
                Log.d(TAG, "success");
                mDialog.dismiss();
                mCurrentRates = response.body().getRates();
                bindAwayCountry(mCountries.getCountryByCurrencyCode(mPreferences.getAwayCountry()));
                bindBaseCountry();
                bindDate(response.body().getTimestamp());
                if (mSwitched)
                    switchCountries();

                if (mPreferences.getAmount() != "0")
                    mBaseCountryAmount.setText(mPreferences.getAmount());

                if (mPreferences.isFirstTime())
                    mPreferences.saveFirstTime();

                getCountryInfo();
            }

            @Override
            public void onFailure(Call<RatesResponse> call, Throwable t) {
                Log.d(TAG, "fail");
                if (mDialog != null)
                    mDialog.dismiss();
                if (mPreferences.isFirstTime()){
                    //TODO show error "first time internet needed
                    showAlertDialog(FAIL_FIRST_TIME);

                }
                else if (!mPreferences.isFirstTime() && mPreferences.getRates() != null) {
                    //TODO show caution that older rates will be loaded and bind older date
                    showAlertDialog(FAIL_OLD_RATES);
                }
            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();
        saveDataToPrefs();
    }

    private void saveDataToPrefs() {
        if (mPreferences != null && !mPreferences.isFirstTime() && (mDialog == null||!mDialog.isShowing())){
            mPreferences.saveAmount(mBaseCountryAmount.getText().toString());
            mPreferences.saveAwayCountry(mBaseCountry.getShortCurrencyName().equals("CAD") ? mAwayCountry.getShortCurrencyName() : mBaseCountry.getShortCurrencyName());
            mPreferences.saveCurrenciesFlipped(mSwitched);
            mPreferences.saveRates(mCurrentRates);
            mPreferences.saveRatesDate(mRateDate.getText().toString());
        }
    }

    private void initService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mApiService = retrofit.create(CurrencyAPIEndpointsInterface.class);
    }

    @Override
    public void onCountrySelected(Country country) {
        if (!mSwitched)
            bindAwayCountry(country);
        else
            bindBaseCountry(country);
        //convertAmount();
    }

    @OnTextChanged(value = R.id.baseCountryAmount, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    protected void convertAmount() {
        String baseAmount = mBaseCountryAmount.getText().toString();
        if ((baseAmount.equals("") || baseAmount.contains("."))&& mDotButton.isEnabled())
            enableDotButton(false);

        else if (!baseAmount.equals("")){
            double amount = Double.parseDouble(baseAmount);
            double convertedAmount = calculateConvertedAmount(amount);
            mAwayCountryAmount.setText(String.valueOf(String.format("%.2f",convertedAmount)));
            ObjectAnimator.ofFloat(mAwayCountryAmount, "alpha", 1f, 0f, 1f)
                    .setDuration(200)
                    .start();
            if (!baseAmount.contains("."))
                enableDotButton(true);
        }
    }

    private double calculateConvertedAmount(double amount) {
        if (mSwitched)
            return amount / mCurrentRates.getRate(mBaseCountry.getShortCurrencyName());
        else
            return amount * mCurrentRates.getRate(mAwayCountry.getShortCurrencyName());
    }

    private void enableDotButton(boolean enable) {
        if (enable){
            mDotButton.setTextColor(Color.parseColor("#ffffff"));
            mDotButton.setEnabled(true);

        }else {
            mDotButton.setTextColor(Color.parseColor("#333333"));
            mDotButton.setEnabled(false);
        }
    }

    @OnClick({R.id.awayCountryImage, R.id.baseCountryImage})
    protected void awayCountryImageClicked(View v){
        if ((!mSwitched && v.getId() == R.id.awayCountryImage) || (mSwitched && v.getId() == R.id.baseCountryImage)) {
            CountriesPickerDialog dialog = CountriesPickerDialog.newInstance(mCountries.getCountries());
            dialog.show(getSupportFragmentManager(), CURRENCY_PICKER_TAG);
        }
    }

    @OnClick(R.id.share_button)
    protected void shareButtonClicked(){
        //TODO share social media
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        String text = "Canadian Currency Converter: " + String.format("%s?id=%s", PLAYSTORE_BROWSER_LINK, getPackageName()) +"\n\n "+
                "1 " + mBaseCountry.getShortCurrencyName() +" = "+ String.valueOf(String.format("%.2f",calculateConvertedAmount(1))) +" "+ mAwayCountry.getShortCurrencyName();
        if (!(mBaseCountryAmount.getText().toString().equals("")|| mAwayCountryAmount.getText().toString().equals("0.00")))
                text += "\n "+mBaseCountryAmount.getText().toString()+ " " + mBaseCountry.getCountryCurrencyText() +" = "+ mAwayCountryAmount.getText().toString() +" "+ mAwayCountry.getCountryCurrencyText();

        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        startActivity(Intent.createChooser(shareIntent, "Share converted amount!"));

    }

    @OnClick(R.id.info_button)
    protected void infoButtonClicked(){
        startActivity(new Intent(this, InfoActivity.class));
    }

    @OnClick(R.id.refreshRate_button)
    protected void refreshButtonClicked(){
        saveDataToPrefs();
        fetchRatesServiceCall();
    }

    private boolean isratesExpired(String ratesDate)  {
        //TODO Return if date matches todays date
        DateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy");
        Date rateExpireDay;
        Date parsedRatesDate;
        try {
            parsedRatesDate = formatter.parse(ratesDate);
            rateExpireDay = new Date(parsedRatesDate.getTime() + EXPIRY_TIME);
            if (new Date().after(rateExpireDay))
                return true;

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return false;
    }

    @OnClick(R.id.switcher)
    protected void onSwitcherClicked(){
        mSwitched = !mSwitched;
        switchCountries();
        if (!mSwitched)
            ObjectAnimator.ofFloat(mSwitcher,"rotation", 180f, 0f)
                    .setDuration(200)
                    .start();
        else
            ObjectAnimator.ofFloat(mSwitcher,"rotation", 0f, 180f)
                    .setDuration(200)
                    .start();
    }


    private void switchCountries() {
        if (mSwitched) {
            Country baseCountry = mBaseCountry;
            bindBaseCountry(mAwayCountry);
            bindAwayCountry(baseCountry);
        }
        else {
            Country awayCountry = mBaseCountry;
            bindBaseCountry();
            bindAwayCountry(awayCountry);
        }
    }

    private void bindAwayCountry(Country awayCountry) {
        mAwayCountry = awayCountry;
        mAwayCountryImage.setImageResource(mAwayCountry.getFlag());
        mAwayCountryCurrency.setText(mAwayCountry.getShortCurrencyName());
        convertAmount();
    }

    private void bindBaseCountry(Country baseCountry) {
        mBaseCountry = baseCountry;
        mBaseCountryImage.setImageResource(mBaseCountry.getFlag());
        mBaseCountryCurrency.setText(mBaseCountry.getShortCurrencyName());
        convertAmount();
    }

    private void bindBaseCountry(){
        mBaseCountry = mCountries.getCountryByCurrencyCode("CAD");
        bindBaseCountry(mBaseCountry);
    }

    private void bindDate(String date){
        String dateString = date;//"Rate as of " + date;
        mRateDate.setText(dateString);
    }

    private void bindDate(long timestamp){
        Date date = new Date(timestamp*1000);
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy");
        mRateDate.setText(dateFormat.format(date));
    }


    @OnClick({R.id.one_button,R.id.two_button,R.id.three_button,
            R.id.four_button,R.id.five_button,R.id.six_button,
            R.id.seven_button,R.id.eight_button,R.id.nine_button,
            R.id.zero_button, R.id.clear_button, R.id.dot_button})
    protected void numberButtonClick(View v){
        switch (v.getId()){
            case R.id.one_button:
                addDigit("1");
                break;

            case R.id.two_button:
                addDigit("2");
                break;

            case R.id.three_button:
                addDigit("3");
                break;

            case R.id.four_button:
                addDigit("4");
                break;

            case R.id.five_button:
                addDigit("5");
                break;

            case R.id.six_button:
                addDigit("6");
                break;

            case R.id.seven_button:
                addDigit("7");
                break;

            case R.id.eight_button:
                addDigit("8");
                break;

            case R.id.nine_button:
                addDigit("9");
                break;

            case R.id.zero_button:
                addDigit("0");
                break;

            case R.id.clear_button:
                removeDigit();
                break;

            case R.id.dot_button:
                addDigit(".");
                break;
        }
    }

    @OnLongClick(R.id.clear_button)
    protected boolean onClearButtonLongClick(){
        mBaseCountryAmount.setText("");
        mAwayCountryAmount.setText("");
        return true;
    }

    private void addDigit(String number) {
        String baseAmount = mBaseCountryAmount.getText().toString();
        if ((number.equals(".") && (baseAmount.contains(".") || baseAmount.equals(""))) ||
                (!baseAmount.equals("") && baseAmount.contains(".") &&(baseAmount.substring(baseAmount.indexOf(".")).length()==3))
                )
            return;
        else {
            String newAmount;
            if (baseAmount.contains(".") && number.equals("."))
                return;
            else if (baseAmount.equals("0") && !number.equals("."))
                newAmount = number;
            else
                newAmount= baseAmount + number;
            mBaseCountryAmount.setText(newAmount);
        }
    }

    private void removeDigit() {
        String amount = mBaseCountryAmount.getText().toString();
        if(amount.length() > 1 && !amount.equals("0")) {
            String newAmount = amount.substring(0, amount.length() - 1);
            mBaseCountryAmount.setText(newAmount);
        }
        else if(amount.length() == 1 && !amount.equals("0"))
            mBaseCountryAmount.setText("0");
        else
            onClearButtonLongClick();
    }

    private void showAlertDialog(int i){
        mDialog = null;
        switch (i){
            case 1:
                mDialog = mDialogBuilder.setMessage("Fetching Rates").create();
                break;

            case 2:
                mDialog = mDialogBuilder.setMessage("Failed to fetch rates, you need internet on first launch")
                        .setNegativeButton("Exit", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })
                        .setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        fetchRatesServiceCall();
                                    }
                                }

                        ).create();

                break;

            case 3:
                mDialog = mDialogBuilder.setMessage("Failed to fetch rates , showing older rates")
                        .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                restoreFromPrefs();
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        fetchRatesServiceCall();
                                    }
                                }

                        ).create();
                break;

            case 4:
                mDialog = mDialogBuilder.setMessage("Are you you want to quit?")
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        finish();
                                    }
                                }

                        ).create();
                break;

        }
        if (mDialog != null){
            mDialog.setCancelable(false);
            mDialog.show();

        }
    }

    @Override
    public void onBackPressed() {
        showAlertDialog(EXIT_APP);
    }

    public void getCountryInfo() {

    }
}
