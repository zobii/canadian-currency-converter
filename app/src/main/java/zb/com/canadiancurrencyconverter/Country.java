package zb.com.canadiancurrencyconverter;

import java.io.Serializable;

/**
 * Created by zohaibhussain on 2016-05-16.
 */
public class Country implements Serializable {
    private String mName;
    private String mShortCurrencyName;
    private String mCurrencySymbol;
    private int mFlag;

    public Country(String name, String shortCurrencyName, int flag){
        this(name, shortCurrencyName, null, flag);
    }
    public Country(String name, String shortCurrencyName, String currencySymbol, int flag) {
        mName = name;
        mShortCurrencyName = shortCurrencyName;
        mCurrencySymbol = currencySymbol;
        mFlag = flag;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getShortCurrencyName() {
        return mShortCurrencyName;
    }

    public void setShortCurrencyName(String shortCurrencyName) {
        mShortCurrencyName = shortCurrencyName;
    }

    public String getCurrencySymbol() {
        return mCurrencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        mCurrencySymbol = currencySymbol;
    }

    public int getFlag() {
        return mFlag;
    }

    public void setFlag(int flag) {
        mFlag = flag;
    }

    public String getCountryCurrencyText(){
        return mName + " ( "+ mShortCurrencyName +" )";
    }
}
