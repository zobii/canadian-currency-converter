package zb.com.canadiancurrencyconverter;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class InfoActivity extends AppCompatActivity {

    public static final String PLAYSTORE_APP_LINK = "market://details";
    public static final String PLAYSTORE_BROWSER_LINK = "https://play.google.com/store/apps/details";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        setTheme(R.style.AppTheme);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.share_button, R.id.back_button, R.id.rate_button})
    protected void buttonClick(View view){
        switch (view.getId()){
            case (R.id.share_button):
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                String text = "Canadian Currency Converter "+ "\n" + String.format("%s?id=%s", PLAYSTORE_BROWSER_LINK, getPackageName());
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, text);
                startActivity(Intent.createChooser(shareIntent, "Share Canadian Currency App!"));
                break;
            case (R.id.rate_button):
                rateApp();
                break;
            case (R.id.back_button):
                finish();
        }
    }


    public void rateApp()
    {
        try {
            Intent rateIntent = rateIntentForUrl(PLAYSTORE_BROWSER_LINK);
            startActivity(rateIntent);
        }
        catch (ActivityNotFoundException e) {
            Intent rateIntent = rateIntentForUrl(PLAYSTORE_APP_LINK);
            startActivity(rateIntent);
        }
    }
    private Intent rateIntentForUrl(String url)
    {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format("%s?id=%s", url, getPackageName())));
        int flags = Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_MULTIPLE_TASK;
        if (Build.VERSION.SDK_INT >= 21)
            flags |= Intent.FLAG_ACTIVITY_NEW_DOCUMENT;

        else {
            //noinspection deprecation
            flags |= Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET;
        }
        intent.addFlags(flags);
        return intent;
    }
}
