package zb.com.canadiancurrencyconverter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by zohaibhussain on 2016-05-11.
 */
public class Countries {
    static List<Country> mCountries = new ArrayList<>();

    public Countries(List<Country> countries) {
        mCountries = countries;
    }

    public Countries() {
        if (mCountries == null || mCountries.size() == 0)
            mCountries
                    .addAll(Arrays.asList(
                            new Country("United Arab Emirates","AED",R.drawable.uae),
                            new Country("Afghanistan","AFN",R.drawable.afghanistan),
                            new Country("Albania","ALL",R.drawable.albania),
                            new Country("Armenia","AMD",R.drawable.armenia),
                            new Country("Netherlands Antilles","ANG",R.drawable.netherlands),
                            new Country("Angola","AOA",R.drawable.angola),
                            new Country("Argentina","ARS",R.drawable.argentina),
                            new Country("Australia","AUD",R.drawable.australia),
                            new Country("Aruba","AWG",R.drawable.aruba),
                            new Country("Azerbaijan","AZN",R.drawable.azerbaijan),
                            new Country("Bosnia-Herzegovina","BAM",R.drawable.bosnia),
                            new Country("Barbados","BBD",R.drawable.barbados),
                            new Country("Bangladesh","BDT",R.drawable.bangladesh),
                            new Country("Bulgaria","BGN",R.drawable.bulgaria),
                            new Country("Bahrain","BHD",R.drawable.bahrain),
                            new Country("Burundi","BIF",R.drawable.burundi),
                            new Country("Bermuda","BMD",R.drawable.bermuda),
                            new Country("Brunei Darussalam","BND",R.drawable.brunei),
                            new Country("Bolivia","BOB",R.drawable.bolivia),
                            new Country("Brazil","BRL",R.drawable.brazil),
                            new Country("Bahamas","BSD",R.drawable.bahamas),
                            new Country("Bitcoin","BTC",R.drawable.bitcoin),
                            new Country("Bhutan","BTN",R.drawable.bhutan),
                            new Country("Botswana","BWP",R.drawable.botswana),
                            new Country("Belarus","BYR",R.drawable.belarus),
                            new Country("Belize","BZD",R.drawable.belize),
                            new Country("Canada","CAD",R.drawable.canada),
                            new Country("Congo, Dem. Republic","CDF",R.drawable.congo),
                            new Country("Switzerland","CHF",R.drawable.switzerland),
                            new Country("Chile","CLP",R.drawable.chile),
                            new Country("China","CNY",R.drawable.china),
                            new Country("Colombia","COP",R.drawable.colombia),
                            new Country("Costa Rica","CRC",R.drawable.costarica),
                            new Country("Cuba Convertible Peso","CUC",R.drawable.cubaconvertiblepeso),
                            new Country("Cuba","CUP",R.drawable.cuba),
                            new Country("Cape Verde","CVE",R.drawable.capeverde),
                            new Country("Czech Rep.","CZK",R.drawable.czech),
                            new Country("Djibouti","DJF",R.drawable.djibouti),
                            new Country("Denmark","DKK",R.drawable.denmark),
                            new Country("Dominican Republic","DOP",R.drawable.dominica),
                            new Country("Algeria","DZD",R.drawable.algeria),
                            new Country("Egypt","EGP",R.drawable.egypt),
                            new Country("Eritrea","ERN",R.drawable.eritrea),
                            new Country("Ethiopia","ETB",R.drawable.ethiopia),
                            new Country("Europe","EUR",R.drawable.europe),
                            new Country("Fiji","FJD",R.drawable.fiji),
                            new Country("Falkland Islands (Malvinas)","FKP",R.drawable.malbinas),
                            new Country("U.K.","GBP",R.drawable.uk),
                            new Country("Georgia","GEL",R.drawable.georgia),
                            new Country("Guernsey","GGP",R.drawable.guernsey),
                            new Country("Ghana","GHS",R.drawable.ghana),
                            new Country("Gibraltar","GIP",R.drawable.gibraltar),
                            new Country("Gambia","GMD",R.drawable.gambia),
                            new Country("Guinea","GNF",R.drawable.guinea),
                            new Country("Guatemalan Quetzal","GTQ",R.drawable.guatemalanquetzal),
                            new Country("Guyana","GYD",R.drawable.guyana),
                            new Country("Hong Kong","HKD",R.drawable.hongkong),
                            new Country("Honduras","HNL",R.drawable.honduras),
                            new Country("Croatia","HRK",R.drawable.croatia),
                            new Country("Haiti","HTG",R.drawable.haiti),
                            new Country("Hungary","HUF",R.drawable.hungary),
                            new Country("Indonesia","IDR",R.drawable.indonesia),
                            new Country("Israel","ILS",R.drawable.israel),
                            new Country("Isle of Man","IMP",R.drawable.isle),
                            new Country("India","INR",R.drawable.india),
                            new Country("Iraq","IQD",R.drawable.iraq),
                            new Country("Iran","IRR",R.drawable.iran),
                            new Country("Iceland","ISK",R.drawable.iceland),
                            new Country("Jamaica","JMD",R.drawable.jamaica),
                            new Country("Jordan","JOD",R.drawable.jordan),
                            new Country("Japan","JPY",R.drawable.japan),
                            new Country("Kenya","KES",R.drawable.kenya),
                            new Country("Kyrgyzstan","KGS",R.drawable.kyrgyzstan),
                            new Country("Cambodia","KHR",R.drawable.cambodia),
                            new Country("Comoros","KMF",R.drawable.comoros),
                            new Country("Korea-North","KPW",R.drawable.northkorea),
                            new Country("Korea-South","KRW",R.drawable.southkorea),
                            new Country("Kuwait","KWD",R.drawable.kuwait),
                            new Country("Cayman Islands","KYD",R.drawable.caymanislands),
                            new Country("Kazakhstan","KZT",R.drawable.kazakhstan),
                            new Country("Laos","LAK",R.drawable.laos),
                            new Country("Lebanon","LBP",R.drawable.lebanon),
                            new Country("Sri Lanka","LKR",R.drawable.srilanka),
                            new Country("Liberia","LRD",R.drawable.liberia),
                            new Country("Lesotho","LSL",R.drawable.lesotho),
                            new Country("Lithuania","LTL",R.drawable.lithuania),
                            new Country("Latvia","LVL",R.drawable.latvia),
                            new Country("Libya","LYD",R.drawable.libya),
                            new Country("Morocco","MAD",R.drawable.morocco),
                            new Country("Moldova","MDL",R.drawable.moldova),
                            new Country("Macedonia","MKD",R.drawable.macedonia),
                            new Country("Myanmar","MMK",R.drawable.myanmar),
                            new Country("Mongolia","MNT",R.drawable.mongolia),
                            new Country("Macau","MOP",R.drawable.macau),
                            new Country("Mauritania","MRO",R.drawable.mauritania),
                            new Country("Mauritius","MUR",R.drawable.mauritius),
                            new Country("Maldives","MVR",R.drawable.maldives),
                            new Country("Malawi","MWK",R.drawable.malawi),
                            new Country("Mexico","MXN",R.drawable.mexico),
                            new Country("Malaysia","MYR",R.drawable.malaysia),
                            new Country("Mozambique","MZN",R.drawable.mozambique),
                            new Country("Namibia","NAD",R.drawable.namibia),
                            new Country("Nigeria","NGN",R.drawable.nigeria),
                            new Country("Nicaragua","NIO",R.drawable.nicaragua),
                            new Country("Norway","NOK",R.drawable.norway),
                            new Country("Nepal","NPR",R.drawable.nepal),
                            new Country("New Zealand","NZD",R.drawable.newzealand),
                            new Country("Oman","OMR",R.drawable.oman),
                            new Country("Panama","PAB",R.drawable.panama),
                            new Country("Peru","PEN",R.drawable.peru),
                            new Country("Papua New Guinea","PGK",R.drawable.papuanewguinea),
                            new Country("Philippines","PHP",R.drawable.philippines),
                            new Country("Pakistan","PKR",R.drawable.pakistan),
                            new Country("Poland","PLN",R.drawable.poland),
                            new Country("Paraguay","PYG",R.drawable.paraguay),
                            new Country("Qatar","QAR",R.drawable.qatar),
                            new Country("Romania","RON",R.drawable.romania),
                            new Country("Serbia","RSD",R.drawable.serbia),
                            new Country("Russia","RUB",R.drawable.russia),
                            new Country("Rwanda","RWF",R.drawable.rwanda),
                            new Country("Saudi Arabia","SAR",R.drawable.saudiarabia),
                            new Country("Solomon Islands","SBD",R.drawable.solomonislands),
                            new Country("Seychelles","SCR",R.drawable.seychelles),
                            new Country("Sudan","SDG",R.drawable.sudan),
                            new Country("Sweden","SEK",R.drawable.sweden),
                            new Country("Singapore","SGD",R.drawable.singapore),
                            new Country("Sierra Leone","SLL",R.drawable.sierraleone),
                            new Country("Somalia","SOS",R.drawable.somalia),
                            new Country("Suriname","SRD",R.drawable.suriname),
                            new Country("Sao Tome and Principe","STD",R.drawable.saotome),
                            new Country("El Salvador","SVC",R.drawable.elsalvador),
                            new Country("Syria","SYP",R.drawable.syria),
                            new Country("Swaziland","SZL",R.drawable.swaziland),
                            new Country("Thailand","THB",R.drawable.thailand),
                            new Country("Tajikistan","TJS",R.drawable.tajikistan),
                            new Country("Turkmenistan","TMT",R.drawable.turkmenistan),
                            new Country("Tunisia","TND",R.drawable.tunisia),
                            new Country("Tonga","TOP",R.drawable.tonga),
                            new Country("Turkey","TRY",R.drawable.turkey),
                            new Country("Trinidad and Tobago","TTD",R.drawable.trinidad),
                            new Country("Taiwan","TWD",R.drawable.taiwan),
                            new Country("Tanzania","TZS",R.drawable.tanzania),
                            new Country("Ukraine","UAH",R.drawable.ukraine),
                            new Country("Uganda","UGX",R.drawable.uganda),
                            new Country("USA","USD",R.drawable.usa),
                            new Country("Uruguay","UYU",R.drawable.uruguay),
                            new Country("Uzbekistan","UZS",R.drawable.uzbekistan),
                            new Country("Venezuela","VEF",R.drawable.venezuela),
                            new Country("Vietnam","VND",R.drawable.vietnam),
                            new Country("Vanuatu","VUV",R.drawable.vanuatu),
                            new Country("Samoa","WST",R.drawable.samoa),
                            new Country("Gabon","XAF",R.drawable.gabon),
                            new Country("Silver","XAG",R.drawable.silver),
                            new Country("Gold","XAU",R.drawable.gold),
                            new Country("Saint Vincent & Grenadines","XCD",R.drawable.saintvincent),
                            new Country("Togo","XOF",R.drawable.togo),
                            new Country("Yemen","YER",R.drawable.yemen),
                            new Country("South Africa","ZAR",R.drawable.southafrica),
                            new Country("Zambia","ZMW",R.drawable.zambia)
                ));
    }

    public List<Country> getCountries() {
        return mCountries;
    }

    public void setCountries(List<Country> countries) {
        mCountries = countries;
    }

    public Country getCountryByCurrencyCode(String currencyCode){
        for (Country c : mCountries)
            if (c.getShortCurrencyName().equals(currencyCode))
                return c;
        return null;
    }

    public static List<Country> getFilteredCountriesList(String filter){
        List<Country> filteredCountryList = new ArrayList<>();
        for (Country c: mCountries)
            if ((c.getName().toLowerCase().contains(filter.toLowerCase()) || c.getShortCurrencyName().toLowerCase().contains(filter.toLowerCase())
                    && !filteredCountryList.contains(c)))
                filteredCountryList.add(c);
        return filteredCountryList;
    }


}
