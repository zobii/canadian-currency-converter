package zb.com.canadiancurrencyconverter;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import zb.com.canadiancurrencyconverter.api.models.Rates;

/**
 * Created by zohaibhussain on 2016-05-15.
 */
public class CurrencyPrefs {
    public static final String PREF_NAME = "currency_prefs";
    public static final String DEFAULT_AWAY_COUNTRY = "USD";

    public static final String CURRENCIES_FLIPPED = "currency_flipped";
    public static final String RATES_DATES = "date_rates";
    public static final String RATES = "rates";
    public static final String AWAY_COUNTRY = "away_country";
    public static final String AMOUNT = "amount";
    public static final String FIRST_TIME = "first_time";


    private static CurrencyPrefs mCurrencyPrefs;
    private SharedPreferences mSharedPreferences;

    public static CurrencyPrefs getInstance(Context context){
        if (mCurrencyPrefs == null)
            mCurrencyPrefs = new CurrencyPrefs(context);
        return mCurrencyPrefs;
    }

    private CurrencyPrefs(Context context) {
        mSharedPreferences = context.getApplicationContext().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public void saveCurrenciesFlipped(boolean flipped){
            mSharedPreferences.edit()
                    .putBoolean(CURRENCIES_FLIPPED, flipped)
                    .apply();
    }

    public boolean getCurrenciesFlipped(){
        if (mSharedPreferences != null)
            return mSharedPreferences.getBoolean(CURRENCIES_FLIPPED, false);
        return false;
    }

    public void saveRatesDate(String date){
        mSharedPreferences.edit()
                .putString(RATES_DATES, date)
                .apply();
    }

    public String getRatesDate(){
        if (mSharedPreferences != null)
            return mSharedPreferences.getString(RATES_DATES, "1970-01-01");
        return null;
    }


    public void saveRates(Rates rates){
        mSharedPreferences.edit()
                .putString(RATES, new Gson().toJson(rates))
                .apply();
    }

    public Rates getRates(){
        if (mSharedPreferences != null){
            String serializedRates = mSharedPreferences.getString(RATES, null);
            if (serializedRates != null)
                return new Gson().fromJson(serializedRates, new TypeToken<Rates>(){}.getType());
        }
        return null;
    }

    public void saveAwayCountry(String countryCurrencyCode){
        mSharedPreferences.edit()
                .putString(AWAY_COUNTRY, countryCurrencyCode)
                .apply();
    }

    public String getAwayCountry(){
        if (mSharedPreferences != null){
            return mSharedPreferences.getString(AWAY_COUNTRY, DEFAULT_AWAY_COUNTRY);
        }
        return DEFAULT_AWAY_COUNTRY;
    }

    public void saveFirstTime(){
        mSharedPreferences.edit()
                .putBoolean(FIRST_TIME, false)
                .apply();
    }
    public boolean isFirstTime(){
        if (mSharedPreferences != null)
            return mSharedPreferences.getBoolean(FIRST_TIME, true);
        return false;
    }

    public void saveAmount(String amount){
        mSharedPreferences.edit()
                .putString(AMOUNT, amount)
                .apply();
    }
    public String getAmount() {
        if (mSharedPreferences != null)
            return mSharedPreferences.getString(AMOUNT, "0");
        return null;
    }
}
