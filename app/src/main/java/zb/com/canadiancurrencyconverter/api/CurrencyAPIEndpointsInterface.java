package zb.com.canadiancurrencyconverter.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by zohaibhussain on 2016-05-11.
 */
public interface CurrencyAPIEndpointsInterface {

    /*@GET("latest")
    Call<RatesResponse> getRates(@Query("base") String currency);*/

    @GET("live")
    Call<RatesResponse> getRates(@Query("access_key") String accessKey);
}
