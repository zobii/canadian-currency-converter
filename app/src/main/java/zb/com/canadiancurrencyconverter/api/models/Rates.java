package zb.com.canadiancurrencyconverter.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by zohaibhussain on 2016-05-11.
 */
public class Rates {

    @SerializedName("USDAED")
    @Expose
    private Double AED;
    @SerializedName("USDAFN")
    @Expose
    private Double AFN;
    @SerializedName("USDALL")
    @Expose
    private Double ALL;
    @SerializedName("USDAMD")
    @Expose
    private Double AMD;
    @SerializedName("USDANG")
    @Expose
    private Double ANG;
    @SerializedName("USDAOA")
    @Expose
    private Double AOA;
    @SerializedName("USDARS")
    @Expose
    private Double ARS;
    @SerializedName("USDAUD")
    @Expose
    private Double AUD;
    @SerializedName("USDAWG")
    @Expose
    private Double AWG;
    @SerializedName("USDAZN")
    @Expose
    private Double AZN;
    @SerializedName("USDBAM")
    @Expose
    private Double BAM;
    @SerializedName("USDBBD")
    @Expose
    private Double BBD;
    @SerializedName("USDBDT")
    @Expose
    private Double BDT;
    @SerializedName("USDBGN")
    @Expose
    private Double BGN;
    @SerializedName("USDBHD")
    @Expose
    private Double BHD;
    @SerializedName("USDBIF")
    @Expose
    private Double BIF;
    @SerializedName("USDBMD")
    @Expose
    private Double BMD;
    @SerializedName("USDBND")
    @Expose
    private Double BND;
    @SerializedName("USDBOB")
    @Expose
    private Double BOB;
    @SerializedName("USDBRL")
    @Expose
    private Double BRL;
    @SerializedName("USDBSD")
    @Expose
    private Double BSD;
    @SerializedName("USDBTC")
    @Expose
    private Double BTC;
    @SerializedName("USDBTN")
    @Expose
    private Double BTN;
    @SerializedName("USDBWP")
    @Expose
    private Double BWP;
    @SerializedName("USDBYR")
    @Expose
    private Double BYR;
    @SerializedName("USDBZD")
    @Expose
    private Double BZD;
    @SerializedName("USDCAD")
    @Expose
    private Double CAD;
    @SerializedName("USDCDF")
    @Expose
    private Double CDF;
    @SerializedName("USDCHF")
    @Expose
    private Double CHF;
    @SerializedName("USDCLF")
    @Expose
    private Double CLF;
    @SerializedName("USDCLP")
    @Expose
    private Double CLP;
    @SerializedName("USDCNY")
    @Expose
    private Double CNY;
    @SerializedName("USDCOP")
    @Expose
    private Double COP;
    @SerializedName("USDCRC")
    @Expose
    private Double CRC;
    @SerializedName("USDCUC")
    @Expose
    private Double CUC;
    @SerializedName("USDCUP")
    @Expose
    private Double CUP;
    @SerializedName("USDCVE")
    @Expose
    private Double CVE;
    @SerializedName("USDCZK")
    @Expose
    private Double CZK;
    @SerializedName("USDDJF")
    @Expose
    private Double DJF;
    @SerializedName("USDDKK")
    @Expose
    private Double DKK;
    @SerializedName("USDDOP")
    @Expose
    private Double DOP;
    @SerializedName("USDDZD")
    @Expose
    private Double DZD;
    @SerializedName("USDEEK")
    @Expose
    private Double EEK;
    @SerializedName("USDEGP")
    @Expose
    private Double EGP;
    @SerializedName("USDERN")
    @Expose
    private Double ERN;
    @SerializedName("USDETB")
    @Expose
    private Double ETB;
    @SerializedName("USDEUR")
    @Expose
    private Double EUR;
    @SerializedName("USDFJD")
    @Expose
    private Double FJD;
    @SerializedName("USDFKP")
    @Expose
    private Double FKP;
    @SerializedName("USDGBP")
    @Expose
    private Double GBP;
    @SerializedName("USDGEL")
    @Expose
    private Double GEL;
    @SerializedName("USDGGP")
    @Expose
    private Double GGP;
    @SerializedName("USDGHS")
    @Expose
    private Double GHS;
    @SerializedName("USDGIP")
    @Expose
    private Double GIP;
    @SerializedName("USDGMD")
    @Expose
    private Double GMD;
    @SerializedName("USDGNF")
    @Expose
    private Double GNF;
    @SerializedName("USDGTQ")
    @Expose
    private Double GTQ;
    @SerializedName("USDGYD")
    @Expose
    private Double GYD;
    @SerializedName("USDHKD")
    @Expose
    private Double HKD;
    @SerializedName("USDHNL")
    @Expose
    private Double HNL;
    @SerializedName("USDHRK")
    @Expose
    private Double HRK;
    @SerializedName("USDHTG")
    @Expose
    private Double HTG;
    @SerializedName("USDHUF")
    @Expose
    private Double HUF;
    @SerializedName("USDIDR")
    @Expose
    private Double IDR;
    @SerializedName("USDILS")
    @Expose
    private Double ILS;
    @SerializedName("USDIMP")
    @Expose
    private Double IMP;
    @SerializedName("USDINR")
    @Expose
    private Double INR;
    @SerializedName("USDIQD")
    @Expose
    private Double IQD;
    @SerializedName("USDIRR")
    @Expose
    private Double IRR;
    @SerializedName("USDISK")
    @Expose
    private Double ISK;
    @SerializedName("USDJEP")
    @Expose
    private Double JEP;
    @SerializedName("USDJMD")
    @Expose
    private Double JMD;
    @SerializedName("USDJOD")
    @Expose
    private Double JOD;
    @SerializedName("USDJPY")
    @Expose
    private Double JPY;
    @SerializedName("USDKES")
    @Expose
    private Double KES;
    @SerializedName("USDKGS")
    @Expose
    private Double KGS;
    @SerializedName("USDKHR")
    @Expose
    private Double KHR;
    @SerializedName("USDKMF")
    @Expose
    private Double KMF;
    @SerializedName("USDKPW")
    @Expose
    private Double KPW;
    @SerializedName("USDKRW")
    @Expose
    private Double KRW;
    @SerializedName("USDKWD")
    @Expose
    private Double KWD;
    @SerializedName("USDKYD")
    @Expose
    private Double KYD;
    @SerializedName("USDKZT")
    @Expose
    private Double KZT;
    @SerializedName("USDLAK")
    @Expose
    private Double LAK;
    @SerializedName("USDLBP")
    @Expose
    private Double LBP;
    @SerializedName("USDLKR")
    @Expose
    private Double LKR;
    @SerializedName("USDLRD")
    @Expose
    private Double LRD;
    @SerializedName("USDLSL")
    @Expose
    private Double LSL;
    @SerializedName("USDLTL")
    @Expose
    private Double LTL;
    @SerializedName("USDLVL")
    @Expose
    private Double LVL;
    @SerializedName("USDLYD")
    @Expose
    private Double LYD;
    @SerializedName("USDMAD")
    @Expose
    private Double MAD;
    @SerializedName("USDMDL")
    @Expose
    private Double MDL;
    @SerializedName("USDMGA")
    @Expose
    private Double MGA;
    @SerializedName("USDMKD")
    @Expose
    private Double MKD;
    @SerializedName("USDMMK")
    @Expose
    private Double MMK;
    @SerializedName("USDMNT")
    @Expose
    private Double MNT;
    @SerializedName("USDMOP")
    @Expose
    private Double MOP;
    @SerializedName("USDMRO")
    @Expose
    private Double MRO;
    @SerializedName("USDMUR")
    @Expose
    private Double MUR;
    @SerializedName("USDMVR")
    @Expose
    private Double MVR;
    @SerializedName("USDMWK")
    @Expose
    private Double MWK;
    @SerializedName("USDMXN")
    @Expose
    private Double MXN;
    @SerializedName("USDMYR")
    @Expose
    private Double MYR;
    @SerializedName("USDMZN")
    @Expose
    private Double MZN;
    @SerializedName("USDNAD")
    @Expose
    private Double NAD;
    @SerializedName("USDNGN")
    @Expose
    private Double NGN;
    @SerializedName("USDNIO")
    @Expose
    private Double NIO;
    @SerializedName("USDNOK")
    @Expose
    private Double NOK;
    @SerializedName("USDNPR")
    @Expose
    private Double NPR;
    @SerializedName("USDNZD")
    @Expose
    private Double NZD;
    @SerializedName("USDOMR")
    @Expose
    private Double OMR;
    @SerializedName("USDPAB")
    @Expose
    private Double PAB;
    @SerializedName("USDPEN")
    @Expose
    private Double PEN;
    @SerializedName("USDPGK")
    @Expose
    private Double PGK;
    @SerializedName("USDPHP")
    @Expose
    private Double PHP;
    @SerializedName("USDPKR")
    @Expose
    private Double PKR;
    @SerializedName("USDPLN")
    @Expose
    private Double PLN;
    @SerializedName("USDPYG")
    @Expose
    private Double PYG;
    @SerializedName("USDQAR")
    @Expose
    private Double QAR;
    @SerializedName("USDRON")
    @Expose
    private Double RON;
    @SerializedName("USDRSD")
    @Expose
    private Double RSD;
    @SerializedName("USDRUB")
    @Expose
    private Double RUB;
    @SerializedName("USDRWF")
    @Expose
    private Double RWF;
    @SerializedName("USDSAR")
    @Expose
    private Double SAR;
    @SerializedName("USDSBD")
    @Expose
    private Double SBD;
    @SerializedName("USDSCR")
    @Expose
    private Double SCR;
    @SerializedName("USDSDG")
    @Expose
    private Double SDG;
    @SerializedName("USDSEK")
    @Expose
    private Double SEK;
    @SerializedName("USDSGD")
    @Expose
    private Double SGD;
    @SerializedName("USDSHP")
    @Expose
    private Double SHP;
    @SerializedName("USDSLL")
    @Expose
    private Double SLL;
    @SerializedName("USDSOS")
    @Expose
    private Double SOS;
    @SerializedName("USDSRD")
    @Expose
    private Double SRD;
    @SerializedName("USDSTD")
    @Expose
    private Double STD;
    @SerializedName("USDSVC")
    @Expose
    private Double SVC;
    @SerializedName("USDSYP")
    @Expose
    private Double SYP;
    @SerializedName("USDSZL")
    @Expose
    private Double SZL;
    @SerializedName("USDTHB")
    @Expose
    private Double THB;
    @SerializedName("USDTJS")
    @Expose
    private Double TJS;
    @SerializedName("USDTMT")
    @Expose
    private Double TMT;
    @SerializedName("USDTND")
    @Expose
    private Double TND;
    @SerializedName("USDTOP")
    @Expose
    private Double TOP;
    @SerializedName("USDTRY")
    @Expose
    private Double TRY;
    @SerializedName("USDTTD")
    @Expose
    private Double TTD;
    @SerializedName("USDTWD")
    @Expose
    private Double TWD;
    @SerializedName("USDTZS")
    @Expose
    private Double TZS;
    @SerializedName("USDUAH")
    @Expose
    private Double UAH;
    @SerializedName("USDUGX")
    @Expose
    private Double UGX;
    @SerializedName("USDUSD")
    @Expose
    private Double USD;
    @SerializedName("USDUYU")
    @Expose
    private Double UYU;
    @SerializedName("USDUZS")
    @Expose
    private Double UZS;
    @SerializedName("USDVEF")
    @Expose
    private Double VEF;
    @SerializedName("USDVND")
    @Expose
    private Double VND;
    @SerializedName("USDVUV")
    @Expose
    private Double VUV;
    @SerializedName("USDWST")
    @Expose
    private Double WST;
    @SerializedName("USDXAF")
    @Expose
    private Double XAF;
    @SerializedName("USDXAG")
    @Expose
    private Double XAG;
    @SerializedName("USDXAU")
    @Expose
    private Double XAU;
    @SerializedName("USDXCD")
    @Expose
    private Double XCD;
    @SerializedName("USDXDR")
    @Expose
    private Double XDR;
    @SerializedName("USDXOF")
    @Expose
    private Double XOF;
    @SerializedName("USDXPF")
    @Expose
    private Double XPF;
    @SerializedName("USDYER")
    @Expose
    private Double YER;
    @SerializedName("USDZAR")
    @Expose
    private Double ZAR;
    @SerializedName("USDZMK")
    @Expose
    private Double ZMK;
    @SerializedName("USDZMW")
    @Expose
    private Double ZMW;
    @SerializedName("USDZWL")
    @Expose
    private Double ZWL;

    public double getRate(String shortCurrencyName) {
        Method[] countryMethods = this.getClass().getDeclaredMethods();
        for (Method method : countryMethods) {
            if (method.getName().startsWith("get") && method.getName().endsWith(shortCurrencyName))
                try {
                    Double rateFromUSD = (Double) method.invoke(this);
                    return rateFromCAD(rateFromUSD);
                } catch (Exception e) {
                    e.printStackTrace();
                    return 0;
                }
        }
        return 0;
    }

    private double rateFromCAD(Double rateFromUSD) {
        return rateFromUSD / getCAD();
    }

    /**
     *
     * @return
     * The AED
     */
    public Double getAED() {
        return AED;
    }

    /**
     *
     * @param AED
     * The AED
     */
    public void setAED(Double AED) {
        this.AED = AED;
    }

    /**
     *
     * @return
     * The AFN
     */
    public Double getAFN() {
        return AFN;
    }

    /**
     *
     * @param AFN
     * The AFN
     */
    public void setAFN(Double AFN) {
        this.AFN = AFN;
    }

    /**
     *
     * @return
     * The ALL
     */
    public Double getALL() {
        return ALL;
    }

    /**
     *
     * @param ALL
     * The ALL
     */
    public void setALL(Double ALL) {
        this.ALL = ALL;
    }

    /**
     *
     * @return
     * The AMD
     */
    public Double getAMD() {
        return AMD;
    }

    /**
     *
     * @param AMD
     * The AMD
     */
    public void setAMD(Double AMD) {
        this.AMD = AMD;
    }

    /**
     *
     * @return
     * The ANG
     */
    public Double getANG() {
        return ANG;
    }

    /**
     *
     * @param ANG
     * The ANG
     */
    public void setANG(Double ANG) {
        this.ANG = ANG;
    }

    /**
     *
     * @return
     * The AOA
     */
    public Double getAOA() {
        return AOA;
    }

    /**
     *
     * @param AOA
     * The AOA
     */
    public void setAOA(Double AOA) {
        this.AOA = AOA;
    }

    /**
     *
     * @return
     * The ARS
     */
    public Double getARS() {
        return ARS;
    }

    /**
     *
     * @param ARS
     * The ARS
     */
    public void setARS(Double ARS) {
        this.ARS = ARS;
    }

    /**
     *
     * @return
     * The AUD
     */
    public Double getAUD() {
        return AUD;
    }

    /**
     *
     * @param AUD
     * The AUD
     */
    public void setAUD(Double AUD) {
        this.AUD = AUD;
    }

    /**
     *
     * @return
     * The AWG
     */
    public Double getAWG() {
        return AWG;
    }

    /**
     *
     * @param AWG
     * The AWG
     */
    public void setAWG(Double AWG) {
        this.AWG = AWG;
    }

    /**
     *
     * @return
     * The AZN
     */
    public Double getAZN() {
        return AZN;
    }

    /**
     *
     * @param AZN
     * The AZN
     */
    public void setAZN(Double AZN) {
        this.AZN = AZN;
    }

    /**
     *
     * @return
     * The BAM
     */
    public Double getBAM() {
        return BAM;
    }

    /**
     *
     * @param BAM
     * The BAM
     */
    public void setBAM(Double BAM) {
        this.BAM = BAM;
    }

    /**
     *
     * @return
     * The BBD
     */
    public Double getBBD() {
        return BBD;
    }

    /**
     *
     * @param BBD
     * The BBD
     */
    public void setBBD(Double BBD) {
        this.BBD = BBD;
    }

    /**
     *
     * @return
     * The BDT
     */
    public Double getBDT() {
        return BDT;
    }

    /**
     *
     * @param BDT
     * The BDT
     */
    public void setBDT(Double BDT) {
        this.BDT = BDT;
    }

    /**
     *
     * @return
     * The BGN
     */
    public Double getBGN() {
        return BGN;
    }

    /**
     *
     * @param BGN
     * The BGN
     */
    public void setBGN(Double BGN) {
        this.BGN = BGN;
    }

    /**
     *
     * @return
     * The BHD
     */
    public Double getBHD() {
        return BHD;
    }

    /**
     *
     * @param BHD
     * The BHD
     */
    public void setBHD(Double BHD) {
        this.BHD = BHD;
    }

    /**
     *
     * @return
     * The BIF
     */
    public Double getBIF() {
        return BIF;
    }

    /**
     *
     * @param BIF
     * The BIF
     */
    public void setBIF(Double BIF) {
        this.BIF = BIF;
    }

    /**
     *
     * @return
     * The BMD
     */
    public Double getBMD() {
        return BMD;
    }

    /**
     *
     * @param BMD
     * The BMD
     */
    public void setBMD(Double BMD) {
        this.BMD = BMD;
    }

    /**
     *
     * @return
     * The BND
     */
    public Double getBND() {
        return BND;
    }

    /**
     *
     * @param BND
     * The BND
     */
    public void setBND(Double BND) {
        this.BND = BND;
    }

    /**
     *
     * @return
     * The BOB
     */
    public Double getBOB() {
        return BOB;
    }

    /**
     *
     * @param BOB
     * The BOB
     */
    public void setBOB(Double BOB) {
        this.BOB = BOB;
    }

    /**
     *
     * @return
     * The BRL
     */
    public Double getBRL() {
        return BRL;
    }

    /**
     *
     * @param BRL
     * The BRL
     */
    public void setBRL(Double BRL) {
        this.BRL = BRL;
    }

    /**
     *
     * @return
     * The BSD
     */
    public Double getBSD() {
        return BSD;
    }

    /**
     *
     * @param BSD
     * The BSD
     */
    public void setBSD(Double BSD) {
        this.BSD = BSD;
    }

    /**
     *
     * @return
     * The BTC
     */
    public Double getBTC() {
        return BTC;
    }

    /**
     *
     * @param BTC
     * The BTC
     */
    public void setBTC(Double BTC) {
        this.BTC = BTC;
    }

    /**
     *
     * @return
     * The BTN
     */
    public Double getBTN() {
        return BTN;
    }

    /**
     *
     * @param BTN
     * The BTN
     */
    public void setBTN(Double BTN) {
        this.BTN = BTN;
    }

    /**
     *
     * @return
     * The BWP
     */
    public Double getBWP() {
        return BWP;
    }

    /**
     *
     * @param BWP
     * The BWP
     */
    public void setBWP(Double BWP) {
        this.BWP = BWP;
    }

    /**
     *
     * @return
     * The BYR
     */
    public Double getBYR() {
        return BYR;
    }

    /**
     *
     * @param BYR
     * The BYR
     */
    public void setBYR(Double BYR) {
        this.BYR = BYR;
    }

    /**
     *
     * @return
     * The BZD
     */
    public Double getBZD() {
        return BZD;
    }

    /**
     *
     * @param BZD
     * The BZD
     */
    public void setBZD(Double BZD) {
        this.BZD = BZD;
    }

    /**
     *
     * @return
     * The CAD
     */
    public Double getCAD() {
        return CAD;
    }

    /**
     *
     * @param CAD
     * The CAD
     */
    public void setCAD(Double CAD) {
        this.CAD = CAD;
    }

    /**
     *
     * @return
     * The CDF
     */
    public Double getCDF() {
        return CDF;
    }

    /**
     *
     * @param CDF
     * The CDF
     */
    public void setCDF(Double CDF) {
        this.CDF = CDF;
    }

    /**
     *
     * @return
     * The CHF
     */
    public Double getCHF() {
        return CHF;
    }

    /**
     *
     * @param CHF
     * The CHF
     */
    public void setCHF(Double CHF) {
        this.CHF = CHF;
    }

    /**
     *
     * @return
     * The CLF
     */
    public Double getCLF() {
        return CLF;
    }

    /**
     *
     * @param CLF
     * The CLF
     */
    public void setCLF(Double CLF) {
        this.CLF = CLF;
    }

    /**
     *
     * @return
     * The CLP
     */
    public Double getCLP() {
        return CLP;
    }

    /**
     *
     * @param CLP
     * The CLP
     */
    public void setCLP(Double CLP) {
        this.CLP = CLP;
    }

    /**
     *
     * @return
     * The CNY
     */
    public Double getCNY() {
        return CNY;
    }

    /**
     *
     * @param CNY
     * The CNY
     */
    public void setCNY(Double CNY) {
        this.CNY = CNY;
    }

    /**
     *
     * @return
     * The COP
     */
    public Double getCOP() {
        return COP;
    }

    /**
     *
     * @param COP
     * The COP
     */
    public void setCOP(Double COP) {
        this.COP = COP;
    }

    /**
     *
     * @return
     * The CRC
     */
    public Double getCRC() {
        return CRC;
    }

    /**
     *
     * @param CRC
     * The CRC
     */
    public void setCRC(Double CRC) {
        this.CRC = CRC;
    }

    /**
     *
     * @return
     * The CUC
     */
    public Double getCUC() {
        return CUC;
    }

    /**
     *
     * @param CUC
     * The CUC
     */
    public void setCUC(Double CUC) {
        this.CUC = CUC;
    }

    /**
     *
     * @return
     * The CUP
     */
    public Double getCUP() {
        return CUP;
    }

    /**
     *
     * @param CUP
     * The CUP
     */
    public void setCUP(Double CUP) {
        this.CUP = CUP;
    }

    /**
     *
     * @return
     * The CVE
     */
    public Double getCVE() {
        return CVE;
    }

    /**
     *
     * @param CVE
     * The CVE
     */
    public void setCVE(Double CVE) {
        this.CVE = CVE;
    }

    /**
     *
     * @return
     * The CZK
     */
    public Double getCZK() {
        return CZK;
    }

    /**
     *
     * @param CZK
     * The CZK
     */
    public void setCZK(Double CZK) {
        this.CZK = CZK;
    }

    /**
     *
     * @return
     * The DJF
     */
    public Double getDJF() {
        return DJF;
    }

    /**
     *
     * @param DJF
     * The DJF
     */
    public void setDJF(Double DJF) {
        this.DJF = DJF;
    }

    /**
     *
     * @return
     * The DKK
     */
    public Double getDKK() {
        return DKK;
    }

    /**
     *
     * @param DKK
     * The DKK
     */
    public void setDKK(Double DKK) {
        this.DKK = DKK;
    }

    /**
     *
     * @return
     * The DOP
     */
    public Double getDOP() {
        return DOP;
    }

    /**
     *
     * @param DOP
     * The DOP
     */
    public void setDOP(Double DOP) {
        this.DOP = DOP;
    }

    /**
     *
     * @return
     * The DZD
     */
    public Double getDZD() {
        return DZD;
    }

    /**
     *
     * @param DZD
     * The DZD
     */
    public void setDZD(Double DZD) {
        this.DZD = DZD;
    }

    /**
     *
     * @return
     * The EEK
     */
    public Double getEEK() {
        return EEK;
    }

    /**
     *
     * @param EEK
     * The EEK
     */
    public void setEEK(Double EEK) {
        this.EEK = EEK;
    }

    /**
     *
     * @return
     * The EGP
     */
    public Double getEGP() {
        return EGP;
    }

    /**
     *
     * @param EGP
     * The EGP
     */
    public void setEGP(Double EGP) {
        this.EGP = EGP;
    }

    /**
     *
     * @return
     * The ERN
     */
    public Double getERN() {
        return ERN;
    }

    /**
     *
     * @param ERN
     * The ERN
     */
    public void setERN(Double ERN) {
        this.ERN = ERN;
    }

    /**
     *
     * @return
     * The ETB
     */
    public Double getETB() {
        return ETB;
    }

    /**
     *
     * @param ETB
     * The ETB
     */
    public void setETB(Double ETB) {
        this.ETB = ETB;
    }

    /**
     *
     * @return
     * The EUR
     */
    public Double getEUR() {
        return EUR;
    }

    /**
     *
     * @param EUR
     * The EUR
     */
    public void setEUR(Double EUR) {
        this.EUR = EUR;
    }

    /**
     *
     * @return
     * The FJD
     */
    public Double getFJD() {
        return FJD;
    }

    /**
     *
     * @param FJD
     * The FJD
     */
    public void setFJD(Double FJD) {
        this.FJD = FJD;
    }

    /**
     *
     * @return
     * The FKP
     */
    public Double getFKP() {
        return FKP;
    }

    /**
     *
     * @param FKP
     * The FKP
     */
    public void setFKP(Double FKP) {
        this.FKP = FKP;
    }

    /**
     *
     * @return
     * The GBP
     */
    public Double getGBP() {
        return GBP;
    }

    /**
     *
     * @param GBP
     * The GBP
     */
    public void setGBP(Double GBP) {
        this.GBP = GBP;
    }

    /**
     *
     * @return
     * The GEL
     */
    public Double getGEL() {
        return GEL;
    }

    /**
     *
     * @param GEL
     * The GEL
     */
    public void setGEL(Double GEL) {
        this.GEL = GEL;
    }

    /**
     *
     * @return
     * The GGP
     */
    public Double getGGP() {
        return GGP;
    }

    /**
     *
     * @param GGP
     * The GGP
     */
    public void setGGP(Double GGP) {
        this.GGP = GGP;
    }

    /**
     *
     * @return
     * The GHS
     */
    public Double getGHS() {
        return GHS;
    }

    /**
     *
     * @param GHS
     * The GHS
     */
    public void setGHS(Double GHS) {
        this.GHS = GHS;
    }

    /**
     *
     * @return
     * The GIP
     */
    public Double getGIP() {
        return GIP;
    }

    /**
     *
     * @param GIP
     * The GIP
     */
    public void setGIP(Double GIP) {
        this.GIP = GIP;
    }

    /**
     *
     * @return
     * The GMD
     */
    public Double getGMD() {
        return GMD;
    }

    /**
     *
     * @param GMD
     * The GMD
     */
    public void setGMD(Double GMD) {
        this.GMD = GMD;
    }

    /**
     *
     * @return
     * The GNF
     */
    public Double getGNF() {
        return GNF;
    }

    /**
     *
     * @param GNF
     * The GNF
     */
    public void setGNF(Double GNF) {
        this.GNF = GNF;
    }

    /**
     *
     * @return
     * The GTQ
     */
    public Double getGTQ() {
        return GTQ;
    }

    /**
     *
     * @param GTQ
     * The GTQ
     */
    public void setGTQ(Double GTQ) {
        this.GTQ = GTQ;
    }

    /**
     *
     * @return
     * The GYD
     */
    public Double getGYD() {
        return GYD;
    }

    /**
     *
     * @param GYD
     * The GYD
     */
    public void setGYD(Double GYD) {
        this.GYD = GYD;
    }

    /**
     *
     * @return
     * The HKD
     */
    public Double getHKD() {
        return HKD;
    }

    /**
     *
     * @param HKD
     * The HKD
     */
    public void setHKD(Double HKD) {
        this.HKD = HKD;
    }

    /**
     *
     * @return
     * The HNL
     */
    public Double getHNL() {
        return HNL;
    }

    /**
     *
     * @param HNL
     * The HNL
     */
    public void setHNL(Double HNL) {
        this.HNL = HNL;
    }

    /**
     *
     * @return
     * The HRK
     */
    public Double getHRK() {
        return HRK;
    }

    /**
     *
     * @param HRK
     * The HRK
     */
    public void setHRK(Double HRK) {
        this.HRK = HRK;
    }

    /**
     *
     * @return
     * The HTG
     */
    public Double getHTG() {
        return HTG;
    }

    /**
     *
     * @param HTG
     * The HTG
     */
    public void setHTG(Double HTG) {
        this.HTG = HTG;
    }

    /**
     *
     * @return
     * The HUF
     */
    public Double getHUF() {
        return HUF;
    }

    /**
     *
     * @param HUF
     * The HUF
     */
    public void setHUF(Double HUF) {
        this.HUF = HUF;
    }

    /**
     *
     * @return
     * The IDR
     */
    public Double getIDR() {
        return IDR;
    }

    /**
     *
     * @param IDR
     * The IDR
     */
    public void setIDR(Double IDR) {
        this.IDR = IDR;
    }

    /**
     *
     * @return
     * The ILS
     */
    public Double getILS() {
        return ILS;
    }

    /**
     *
     * @param ILS
     * The ILS
     */
    public void setILS(Double ILS) {
        this.ILS = ILS;
    }

    /**
     *
     * @return
     * The IMP
     */
    public Double getIMP() {
        return IMP;
    }

    /**
     *
     * @param IMP
     * The IMP
     */
    public void setIMP(Double IMP) {
        this.IMP = IMP;
    }

    /**
     *
     * @return
     * The INR
     */
    public Double getINR() {
        return INR;
    }

    /**
     *
     * @param INR
     * The INR
     */
    public void setINR(Double INR) {
        this.INR = INR;
    }

    /**
     *
     * @return
     * The IQD
     */
    public Double getIQD() {
        return IQD;
    }

    /**
     *
     * @param IQD
     * The IQD
     */
    public void setIQD(Double IQD) {
        this.IQD = IQD;
    }

    /**
     *
     * @return
     * The IRR
     */
    public Double getIRR() {
        return IRR;
    }

    /**
     *
     * @param IRR
     * The IRR
     */
    public void setIRR(Double IRR) {
        this.IRR = IRR;
    }

    /**
     *
     * @return
     * The ISK
     */
    public Double getISK() {
        return ISK;
    }

    /**
     *
     * @param ISK
     * The ISK
     */
    public void setISK(Double ISK) {
        this.ISK = ISK;
    }

    /**
     *
     * @return
     * The JEP
     */
    public Double getJEP() {
        return JEP;
    }

    /**
     *
     * @param JEP
     * The JEP
     */
    public void setJEP(Double JEP) {
        this.JEP = JEP;
    }

    /**
     *
     * @return
     * The JMD
     */
    public Double getJMD() {
        return JMD;
    }

    /**
     *
     * @param JMD
     * The JMD
     */
    public void setJMD(Double JMD) {
        this.JMD = JMD;
    }

    /**
     *
     * @return
     * The JOD
     */
    public Double getJOD() {
        return JOD;
    }

    /**
     *
     * @param JOD
     * The JOD
     */
    public void setJOD(Double JOD) {
        this.JOD = JOD;
    }

    /**
     *
     * @return
     * The JPY
     */
    public Double getJPY() {
        return JPY;
    }

    /**
     *
     * @param JPY
     * The JPY
     */
    public void setJPY(Double JPY) {
        this.JPY = JPY;
    }

    /**
     *
     * @return
     * The KES
     */
    public Double getKES() {
        return KES;
    }

    /**
     *
     * @param KES
     * The KES
     */
    public void setKES(Double KES) {
        this.KES = KES;
    }

    /**
     *
     * @return
     * The KGS
     */
    public Double getKGS() {
        return KGS;
    }

    /**
     *
     * @param KGS
     * The KGS
     */
    public void setKGS(Double KGS) {
        this.KGS = KGS;
    }

    /**
     *
     * @return
     * The KHR
     */
    public Double getKHR() {
        return KHR;
    }

    /**
     *
     * @param KHR
     * The KHR
     */
    public void setKHR(Double KHR) {
        this.KHR = KHR;
    }

    /**
     *
     * @return
     * The KMF
     */
    public Double getKMF() {
        return KMF;
    }

    /**
     *
     * @param KMF
     * The KMF
     */
    public void setKMF(Double KMF) {
        this.KMF = KMF;
    }

    /**
     *
     * @return
     * The KPW
     */
    public Double getKPW() {
        return KPW;
    }

    /**
     *
     * @param KPW
     * The KPW
     */
    public void setKPW(Double KPW) {
        this.KPW = KPW;
    }

    /**
     *
     * @return
     * The KRW
     */
    public Double getKRW() {
        return KRW;
    }

    /**
     *
     * @param KRW
     * The KRW
     */
    public void setKRW(Double KRW) {
        this.KRW = KRW;
    }

    /**
     *
     * @return
     * The KWD
     */
    public Double getKWD() {
        return KWD;
    }

    /**
     *
     * @param KWD
     * The KWD
     */
    public void setKWD(Double KWD) {
        this.KWD = KWD;
    }

    /**
     *
     * @return
     * The KYD
     */
    public Double getKYD() {
        return KYD;
    }

    /**
     *
     * @param KYD
     * The KYD
     */
    public void setKYD(Double KYD) {
        this.KYD = KYD;
    }

    /**
     *
     * @return
     * The KZT
     */
    public Double getKZT() {
        return KZT;
    }

    /**
     *
     * @param KZT
     * The KZT
     */
    public void setKZT(Double KZT) {
        this.KZT = KZT;
    }

    /**
     *
     * @return
     * The LAK
     */
    public Double getLAK() {
        return LAK;
    }

    /**
     *
     * @param LAK
     * The LAK
     */
    public void setLAK(Double LAK) {
        this.LAK = LAK;
    }

    /**
     *
     * @return
     * The LBP
     */
    public Double getLBP() {
        return LBP;
    }

    /**
     *
     * @param LBP
     * The LBP
     */
    public void setLBP(Double LBP) {
        this.LBP = LBP;
    }

    /**
     *
     * @return
     * The LKR
     */
    public Double getLKR() {
        return LKR;
    }

    /**
     *
     * @param LKR
     * The LKR
     */
    public void setLKR(Double LKR) {
        this.LKR = LKR;
    }

    /**
     *
     * @return
     * The LRD
     */
    public Double getLRD() {
        return LRD;
    }

    /**
     *
     * @param LRD
     * The LRD
     */
    public void setLRD(Double LRD) {
        this.LRD = LRD;
    }

    /**
     *
     * @return
     * The LSL
     */
    public Double getLSL() {
        return LSL;
    }

    /**
     *
     * @param LSL
     * The LSL
     */
    public void setLSL(Double LSL) {
        this.LSL = LSL;
    }

    /**
     *
     * @return
     * The LTL
     */
    public Double getLTL() {
        return LTL;
    }

    /**
     *
     * @param LTL
     * The LTL
     */
    public void setLTL(Double LTL) {
        this.LTL = LTL;
    }

    /**
     *
     * @return
     * The LVL
     */
    public Double getLVL() {
        return LVL;
    }

    /**
     *
     * @param LVL
     * The LVL
     */
    public void setLVL(Double LVL) {
        this.LVL = LVL;
    }

    /**
     *
     * @return
     * The LYD
     */
    public Double getLYD() {
        return LYD;
    }

    /**
     *
     * @param LYD
     * The LYD
     */
    public void setLYD(Double LYD) {
        this.LYD = LYD;
    }

    /**
     *
     * @return
     * The MAD
     */
    public Double getMAD() {
        return MAD;
    }

    /**
     *
     * @param MAD
     * The MAD
     */
    public void setMAD(Double MAD) {
        this.MAD = MAD;
    }

    /**
     *
     * @return
     * The MDL
     */
    public Double getMDL() {
        return MDL;
    }

    /**
     *
     * @param MDL
     * The MDL
     */
    public void setMDL(Double MDL) {
        this.MDL = MDL;
    }

    /**
     *
     * @return
     * The MGA
     */
    public Double getMGA() {
        return MGA;
    }

    /**
     *
     * @param MGA
     * The MGA
     */
    public void setMGA(Double MGA) {
        this.MGA = MGA;
    }

    /**
     *
     * @return
     * The MKD
     */
    public Double getMKD() {
        return MKD;
    }

    /**
     *
     * @param MKD
     * The MKD
     */
    public void setMKD(Double MKD) {
        this.MKD = MKD;
    }

    /**
     *
     * @return
     * The MMK
     */
    public Double getMMK() {
        return MMK;
    }

    /**
     *
     * @param MMK
     * The MMK
     */
    public void setMMK(Double MMK) {
        this.MMK = MMK;
    }

    /**
     *
     * @return
     * The MNT
     */
    public Double getMNT() {
        return MNT;
    }

    /**
     *
     * @param MNT
     * The MNT
     */
    public void setMNT(Double MNT) {
        this.MNT = MNT;
    }

    /**
     *
     * @return
     * The MOP
     */
    public Double getMOP() {
        return MOP;
    }

    /**
     *
     * @param MOP
     * The MOP
     */
    public void setMOP(Double MOP) {
        this.MOP = MOP;
    }

    /**
     *
     * @return
     * The MRO
     */
    public Double getMRO() {
        return MRO;
    }

    /**
     *
     * @param MRO
     * The MRO
     */
    public void setMRO(Double MRO) {
        this.MRO = MRO;
    }

    /**
     *
     * @return
     * The MUR
     */
    public Double getMUR() {
        return MUR;
    }

    /**
     *
     * @param MUR
     * The MUR
     */
    public void setMUR(Double MUR) {
        this.MUR = MUR;
    }

    /**
     *
     * @return
     * The MVR
     */
    public Double getMVR() {
        return MVR;
    }

    /**
     *
     * @param MVR
     * The MVR
     */
    public void setMVR(Double MVR) {
        this.MVR = MVR;
    }

    /**
     *
     * @return
     * The MWK
     */
    public Double getMWK() {
        return MWK;
    }

    /**
     *
     * @param MWK
     * The MWK
     */
    public void setMWK(Double MWK) {
        this.MWK = MWK;
    }

    /**
     *
     * @return
     * The MXN
     */
    public Double getMXN() {
        return MXN;
    }

    /**
     *
     * @param MXN
     * The MXN
     */
    public void setMXN(Double MXN) {
        this.MXN = MXN;
    }

    /**
     *
     * @return
     * The MYR
     */
    public Double getMYR() {
        return MYR;
    }

    /**
     *
     * @param MYR
     * The MYR
     */
    public void setMYR(Double MYR) {
        this.MYR = MYR;
    }

    /**
     *
     * @return
     * The MZN
     */
    public Double getMZN() {
        return MZN;
    }

    /**
     *
     * @param MZN
     * The MZN
     */
    public void setMZN(Double MZN) {
        this.MZN = MZN;
    }

    /**
     *
     * @return
     * The NAD
     */
    public Double getNAD() {
        return NAD;
    }

    /**
     *
     * @param NAD
     * The NAD
     */
    public void setNAD(Double NAD) {
        this.NAD = NAD;
    }

    /**
     *
     * @return
     * The NGN
     */
    public Double getNGN() {
        return NGN;
    }

    /**
     *
     * @param NGN
     * The NGN
     */
    public void setNGN(Double NGN) {
        this.NGN = NGN;
    }

    /**
     *
     * @return
     * The NIO
     */
    public Double getNIO() {
        return NIO;
    }

    /**
     *
     * @param NIO
     * The NIO
     */
    public void setNIO(Double NIO) {
        this.NIO = NIO;
    }

    /**
     *
     * @return
     * The NOK
     */
    public Double getNOK() {
        return NOK;
    }

    /**
     *
     * @param NOK
     * The NOK
     */
    public void setNOK(Double NOK) {
        this.NOK = NOK;
    }

    /**
     *
     * @return
     * The NPR
     */
    public Double getNPR() {
        return NPR;
    }

    /**
     *
     * @param NPR
     * The NPR
     */
    public void setNPR(Double NPR) {
        this.NPR = NPR;
    }

    /**
     *
     * @return
     * The NZD
     */
    public Double getNZD() {
        return NZD;
    }

    /**
     *
     * @param NZD
     * The NZD
     */
    public void setNZD(Double NZD) {
        this.NZD = NZD;
    }

    /**
     *
     * @return
     * The OMR
     */
    public Double getOMR() {
        return OMR;
    }

    /**
     *
     * @param OMR
     * The OMR
     */
    public void setOMR(Double OMR) {
        this.OMR = OMR;
    }

    /**
     *
     * @return
     * The PAB
     */
    public Double getPAB() {
        return PAB;
    }

    /**
     *
     * @param PAB
     * The PAB
     */
    public void setPAB(Double PAB) {
        this.PAB = PAB;
    }

    /**
     *
     * @return
     * The PEN
     */
    public Double getPEN() {
        return PEN;
    }

    /**
     *
     * @param PEN
     * The PEN
     */
    public void setPEN(Double PEN) {
        this.PEN = PEN;
    }

    /**
     *
     * @return
     * The PGK
     */
    public Double getPGK() {
        return PGK;
    }

    /**
     *
     * @param PGK
     * The PGK
     */
    public void setPGK(Double PGK) {
        this.PGK = PGK;
    }

    /**
     *
     * @return
     * The PHP
     */
    public Double getPHP() {
        return PHP;
    }

    /**
     *
     * @param PHP
     * The PHP
     */
    public void setPHP(Double PHP) {
        this.PHP = PHP;
    }

    /**
     *
     * @return
     * The PKR
     */
    public Double getPKR() {
        return PKR;
    }

    /**
     *
     * @param PKR
     * The PKR
     */
    public void setPKR(Double PKR) {
        this.PKR = PKR;
    }

    /**
     *
     * @return
     * The PLN
     */
    public Double getPLN() {
        return PLN;
    }

    /**
     *
     * @param PLN
     * The PLN
     */
    public void setPLN(Double PLN) {
        this.PLN = PLN;
    }

    /**
     *
     * @return
     * The PYG
     */
    public Double getPYG() {
        return PYG;
    }

    /**
     *
     * @param PYG
     * The PYG
     */
    public void setPYG(Double PYG) {
        this.PYG = PYG;
    }

    /**
     *
     * @return
     * The QAR
     */
    public Double getQAR() {
        return QAR;
    }

    /**
     *
     * @param QAR
     * The QAR
     */
    public void setQAR(Double QAR) {
        this.QAR = QAR;
    }

    /**
     *
     * @return
     * The RON
     */
    public Double getRON() {
        return RON;
    }

    /**
     *
     * @param RON
     * The RON
     */
    public void setRON(Double RON) {
        this.RON = RON;
    }

    /**
     *
     * @return
     * The RSD
     */
    public Double getRSD() {
        return RSD;
    }

    /**
     *
     * @param RSD
     * The RSD
     */
    public void setRSD(Double RSD) {
        this.RSD = RSD;
    }

    /**
     *
     * @return
     * The RUB
     */
    public Double getRUB() {
        return RUB;
    }

    /**
     *
     * @param RUB
     * The RUB
     */
    public void setRUB(Double RUB) {
        this.RUB = RUB;
    }

    /**
     *
     * @return
     * The RWF
     */
    public Double getRWF() {
        return RWF;
    }

    /**
     *
     * @param RWF
     * The RWF
     */
    public void setRWF(Double RWF) {
        this.RWF = RWF;
    }

    /**
     *
     * @return
     * The SAR
     */
    public Double getSAR() {
        return SAR;
    }

    /**
     *
     * @param SAR
     * The SAR
     */
    public void setSAR(Double SAR) {
        this.SAR = SAR;
    }

    /**
     *
     * @return
     * The SBD
     */
    public Double getSBD() {
        return SBD;
    }

    /**
     *
     * @param SBD
     * The SBD
     */
    public void setSBD(Double SBD) {
        this.SBD = SBD;
    }

    /**
     *
     * @return
     * The SCR
     */
    public Double getSCR() {
        return SCR;
    }

    /**
     *
     * @param SCR
     * The SCR
     */
    public void setSCR(Double SCR) {
        this.SCR = SCR;
    }

    /**
     *
     * @return
     * The SDG
     */
    public Double getSDG() {
        return SDG;
    }

    /**
     *
     * @param SDG
     * The SDG
     */
    public void setSDG(Double SDG) {
        this.SDG = SDG;
    }

    /**
     *
     * @return
     * The SEK
     */
    public Double getSEK() {
        return SEK;
    }

    /**
     *
     * @param SEK
     * The SEK
     */
    public void setSEK(Double SEK) {
        this.SEK = SEK;
    }

    /**
     *
     * @return
     * The SGD
     */
    public Double getSGD() {
        return SGD;
    }

    /**
     *
     * @param SGD
     * The SGD
     */
    public void setSGD(Double SGD) {
        this.SGD = SGD;
    }

    /**
     *
     * @return
     * The SHP
     */
    public Double getSHP() {
        return SHP;
    }

    /**
     *
     * @param SHP
     * The SHP
     */
    public void setSHP(Double SHP) {
        this.SHP = SHP;
    }

    /**
     *
     * @return
     * The SLL
     */
    public Double getSLL() {
        return SLL;
    }

    /**
     *
     * @param SLL
     * The SLL
     */
    public void setSLL(Double SLL) {
        this.SLL = SLL;
    }

    /**
     *
     * @return
     * The SOS
     */
    public Double getSOS() {
        return SOS;
    }

    /**
     *
     * @param SOS
     * The SOS
     */
    public void setSOS(Double SOS) {
        this.SOS = SOS;
    }

    /**
     *
     * @return
     * The SRD
     */
    public Double getSRD() {
        return SRD;
    }

    /**
     *
     * @param SRD
     * The SRD
     */
    public void setSRD(Double SRD) {
        this.SRD = SRD;
    }

    /**
     *
     * @return
     * The STD
     */
    public Double getSTD() {
        return STD;
    }

    /**
     *
     * @param STD
     * The STD
     */
    public void setSTD(Double STD) {
        this.STD = STD;
    }

    /**
     *
     * @return
     * The SVC
     */
    public Double getSVC() {
        return SVC;
    }

    /**
     *
     * @param SVC
     * The SVC
     */
    public void setSVC(Double SVC) {
        this.SVC = SVC;
    }

    /**
     *
     * @return
     * The SYP
     */
    public Double getSYP() {
        return SYP;
    }

    /**
     *
     * @param SYP
     * The SYP
     */
    public void setSYP(Double SYP) {
        this.SYP = SYP;
    }

    /**
     *
     * @return
     * The SZL
     */
    public Double getSZL() {
        return SZL;
    }

    /**
     *
     * @param SZL
     * The SZL
     */
    public void setSZL(Double SZL) {
        this.SZL = SZL;
    }

    /**
     *
     * @return
     * The THB
     */
    public Double getTHB() {
        return THB;
    }

    /**
     *
     * @param THB
     * The THB
     */
    public void setTHB(Double THB) {
        this.THB = THB;
    }

    /**
     *
     * @return
     * The TJS
     */
    public Double getTJS() {
        return TJS;
    }

    /**
     *
     * @param TJS
     * The TJS
     */
    public void setTJS(Double TJS) {
        this.TJS = TJS;
    }

    /**
     *
     * @return
     * The TMT
     */
    public Double getTMT() {
        return TMT;
    }

    /**
     *
     * @param TMT
     * The TMT
     */
    public void setTMT(Double TMT) {
        this.TMT = TMT;
    }

    /**
     *
     * @return
     * The TND
     */
    public Double getTND() {
        return TND;
    }

    /**
     *
     * @param TND
     * The TND
     */
    public void setTND(Double TND) {
        this.TND = TND;
    }

    /**
     *
     * @return
     * The TOP
     */
    public Double getTOP() {
        return TOP;
    }

    /**
     *
     * @param TOP
     * The TOP
     */
    public void setTOP(Double TOP) {
        this.TOP = TOP;
    }

    /**
     *
     * @return
     * The TRY
     */
    public Double getTRY() {
        return TRY;
    }

    /**
     *
     * @param TRY
     * The TRY
     */
    public void setTRY(Double TRY) {
        this.TRY = TRY;
    }

    /**
     *
     * @return
     * The TTD
     */
    public Double getTTD() {
        return TTD;
    }

    /**
     *
     * @param TTD
     * The TTD
     */
    public void setTTD(Double TTD) {
        this.TTD = TTD;
    }

    /**
     *
     * @return
     * The TWD
     */
    public Double getTWD() {
        return TWD;
    }

    /**
     *
     * @param TWD
     * The TWD
     */
    public void setTWD(Double TWD) {
        this.TWD = TWD;
    }

    /**
     *
     * @return
     * The TZS
     */
    public Double getTZS() {
        return TZS;
    }

    /**
     *
     * @param TZS
     * The TZS
     */
    public void setTZS(Double TZS) {
        this.TZS = TZS;
    }

    /**
     *
     * @return
     * The UAH
     */
    public Double getUAH() {
        return UAH;
    }

    /**
     *
     * @param UAH
     * The UAH
     */
    public void setUAH(Double UAH) {
        this.UAH = UAH;
    }

    /**
     *
     * @return
     * The UGX
     */
    public Double getUGX() {
        return UGX;
    }

    /**
     *
     * @param UGX
     * The UGX
     */
    public void setUGX(Double UGX) {
        this.UGX = UGX;
    }

    /**
     *
     * @return
     * The 
     */
    public Double getUSD() {
        return USD;
    }

    /**
     *
     * @param USD
     * The USD
     */
    public void setUSD(Double USD) {
        this.USD = USD;
    }

    /**
     *
     * @return
     * The UYU
     */
    public Double getUYU() {
        return UYU;
    }

    /**
     *
     * @param UYU
     * The UYU
     */
    public void setUYU(Double UYU) {
        this.UYU = UYU;
    }

    /**
     *
     * @return
     * The UZS
     */
    public Double getUZS() {
        return UZS;
    }

    /**
     *
     * @param UZS
     * The UZS
     */
    public void setUZS(Double UZS) {
        this.UZS = UZS;
    }

    /**
     *
     * @return
     * The VEF
     */
    public Double getVEF() {
        return VEF;
    }

    /**
     *
     * @param VEF
     * The VEF
     */
    public void setVEF(Double VEF) {
        this.VEF = VEF;
    }

    /**
     *
     * @return
     * The VND
     */
    public Double getVND() {
        return VND;
    }

    /**
     *
     * @param VND
     * The VND
     */
    public void setVND(Double VND) {
        this.VND = VND;
    }

    /**
     *
     * @return
     * The VUV
     */
    public Double getVUV() {
        return VUV;
    }

    /**
     *
     * @param VUV
     * The VUV
     */
    public void setVUV(Double VUV) {
        this.VUV = VUV;
    }

    /**
     *
     * @return
     * The WST
     */
    public Double getWST() {
        return WST;
    }

    /**
     *
     * @param WST
     * The WST
     */
    public void setWST(Double WST) {
        this.WST = WST;
    }

    /**
     *
     * @return
     * The XAF
     */
    public Double getXAF() {
        return XAF;
    }

    /**
     *
     * @param XAF
     * The XAF
     */
    public void setXAF(Double XAF) {
        this.XAF = XAF;
    }

    /**
     *
     * @return
     * The XAG
     */
    public Double getXAG() {
        return XAG;
    }

    /**
     *
     * @param XAG
     * The XAG
     */
    public void setXAG(Double XAG) {
        this.XAG = XAG;
    }

    /**
     *
     * @return
     * The XAU
     */
    public Double getXAU() {
        return XAU;
    }

    /**
     *
     * @param XAU
     * The XAU
     */
    public void setXAU(Double XAU) {
        this.XAU = XAU;
    }

    /**
     *
     * @return
     * The XCD
     */
    public Double getXCD() {
        return XCD;
    }

    /**
     *
     * @param XCD
     * The XCD
     */
    public void setXCD(Double XCD) {
        this.XCD = XCD;
    }

    /**
     *
     * @return
     * The XDR
     */
    public Double getXDR() {
        return XDR;
    }

    /**
     *
     * @param XDR
     * The XDR
     */
    public void setXDR(Double XDR) {
        this.XDR = XDR;
    }

    /**
     *
     * @return
     * The XOF
     */
    public Double getXOF() {
        return XOF;
    }

    /**
     *
     * @param XOF
     * The XOF
     */
    public void setXOF(Double XOF) {
        this.XOF = XOF;
    }

    /**
     *
     * @return
     * The XPF
     */
    public Double getXPF() {
        return XPF;
    }

    /**
     *
     * @param XPF
     * The XPF
     */
    public void setXPF(Double XPF) {
        this.XPF = XPF;
    }

    /**
     *
     * @return
     * The YER
     */
    public Double getYER() {
        return YER;
    }

    /**
     *
     * @param YER
     * The YER
     */
    public void setYER(Double YER) {
        this.YER = YER;
    }

    /**
     *
     * @return
     * The ZAR
     */
    public Double getZAR() {
        return ZAR;
    }

    /**
     *
     * @param ZAR
     * The ZAR
     */
    public void setZAR(Double ZAR) {
        this.ZAR = ZAR;
    }

    /**
     *
     * @return
     * The ZMK
     */
    public Double getZMK() {
        return ZMK;
    }

    /**
     *
     * @param ZMK
     * The ZMK
     */
    public void setZMK(Double ZMK) {
        this.ZMK = ZMK;
    }

    /**
     *
     * @return
     * The ZMW
     */
    public Double getZMW() {
        return ZMW;
    }

    /**
     *
     * @param ZMW
     * The ZMW
     */
    public void setZMW(Double ZMW) {
        this.ZMW = ZMW;
    }

    /**
     *
     * @return
     * The ZWL
     */
    public Double getZWL() {
        return ZWL;
    }

    /**
     *
     * @param ZWL
     * The ZWL
     */
    public void setZWL(Double ZWL) {
        this.ZWL = ZWL;
    }

       /* switch (shortCurrencyName) {
            case "AUD":
                return getAUD();

            case "BGN":
                return getBGN();

            case "BRL":
                return getBRL();

            case "CAD":
                return 1;

            case "CHF":
                return getCHF();

            case "CNY":
                return getCNY();

            case "CZK":
                return getCZK();

            case "DKK":
                return getDKK();

            case "GBP":
                return getGBP();

            case "HKD":
                return getHKD();

            case "HRK":
                return getHRK();

            case "HUF":
                return getHUF();

            case "IDR":
                return getIDR();

            case "ILS":
                return getILS();

            case "INR":
                return getINR();

            case "JPY":
                return getJPY();

            case "KRW":
                return getKRW();

            case "MXN":
                return getMXN();

            case "MYR":
                return getMYR();

            case "NOK":
                return getNOK();

            case "NZD":
                return getNZD();

            case "PHP":
                return getPHP();

            case "PLN":
                return getPLN();

            case "RON":
                return getRON();

            case "RUB":
                return getRUB();

            case "SEK":
                return getSEK();

            case "SGD":
                return getSGD();

            case "THB":
                return getTHB();

            case "TRY":
                return getTRY();

            case "USD":
                return getUSD();

            case "ZAR":
                return getZAR();

            case "EUR":
                return getEUR();

            default:
                return 0;
        }*/

}
